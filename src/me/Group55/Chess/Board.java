package me.Group55.Chess;

import me.Group55.Chess.Pieces.*;

import java.util.ArrayList;

/**
 * Contains information about the game and functions that allow you to move pieces
 *
 * @author Mason Grosset
 * @author Jared Neal
 */
public class Board {
	/**
	 * Points to the white king
	 */
	private King wKing;

	/**
	 * Points to the black king
	 */
	private King bKing;


	/**
	 * An 8*8 array of game pieces on the board
	 */
	private Piece[][] pieces = new Piece[8][8];

	/**
	 * The last moved piece
	 */
	private Piece undoPiece = null;

	/**
	 * Where the last move was from
	 */
	private Coordinate undoCoord = null;

	/**
	 * The piece that was taken last turn
	 */
	private Piece undoKilled = null;

	/**
	 * Did white win
	 */
	boolean wWin = false;

	/**
	 * Did black win
	 */
	boolean bWin = false;

	/**
	 * Was there a draw offer last round
	 */
	boolean draw = false;

	/**
	 * Creates a standard 8*8 board with the pieces in starting positions
	 */
	public Board() {
		for (char c = 'a'; c <= 'h'; c++) {
			addPiece(new Pawn(this, new Coordinate(c, 2), true));
			addPiece(new Pawn(this, new Coordinate(c, 7), false));
			if (c == 'a' || c == 'h') {
				addPiece(new Rook(this, new Coordinate(c, 1), true));
				addPiece(new Rook(this, new Coordinate(c, 8), false));
			} else if (c == 'b' || c == 'g') {
				addPiece(new Knight(this, new Coordinate(c, 1), true));
				addPiece(new Knight(this, new Coordinate(c, 8), false));
			} else if (c == 'c' || c == 'f') {
				addPiece(new Bishop(this, new Coordinate(c, 1), true));
				addPiece(new Bishop(this, new Coordinate(c, 8), false));
			} else if (c == 'd') {
				addPiece(new Queen(this, new Coordinate(c, 1), true));
				addPiece(new Queen(this, new Coordinate(c, 8), false));
			} else if (c == 'e') {
				wKing = new King(this, new Coordinate(c, 1), true);
				bKing = new King(this, new Coordinate(c, 8), false);
				addPiece(wKing);
				addPiece(bKing);
			}
		}
	}

	/**
	 * Creates a test board to test stalemate detection
	 *
	 * @param test Was never implemented
	 */
	public Board(int test) {
		wKing = new King(this, new Coordinate('f', 7), true);
		bKing = new King(this, new Coordinate('h', 8), false);
		addPiece(new Queen(this, new Coordinate('g', 6), true));
		addPiece(wKing);
		addPiece(bKing);
	}

	/**
	 * Makes a move on the board based off of input
	 *
	 * @param input String from console containing move instructions
	 * @param white True if it is currently white's turn
	 * @return Whether the move was successful
	 */
	public boolean tryMove(String input, boolean white) {
		if (input.equals("resign")) {
			setWin(!white);
			return true;
		}
		if (input.equals("draw") && draw) {
			wWin = true;
			bWin = true;
			return true;
		} else draw = false;
		String[] inAr = input.split(" ");
		boolean knight = false;
		boolean queen = false;
		boolean rook = false;
		boolean bishop = false;
		if (inAr.length > 3) return false;
		if (inAr.length == 3)
			if (inAr[2].equals("draw?")) draw = true;
			else if (inAr[2].equals("N")) knight = true;
			else if (inAr[2].equals("Q")) queen = true;
			else if (inAr[2].equals("B")) bishop = true;
			else if (inAr[2].equals("R")) rook = true;
			else return false;

		Coordinate cFrom, cTo;
		try {
			cFrom = new Coordinate(inAr[0].charAt(0), inAr[0].charAt(1) - '0');
			cTo = new Coordinate(inAr[1].charAt(0), inAr[1].charAt(1) - '0');
		} catch (IndexOutOfBoundsException ignored) {
			return false;
		}
		Piece piece = pieceAt(cFrom);
		if (piece == null || piece.isWhite() != white) return false;
		if (!move(piece, cTo)) return false;
		clearEnPassant(!white);
		if (piece instanceof King) {
			if (cFrom.x - cTo.x == -2)
				move(pieceAt(cTo.add(1, 0)), cFrom.add(1, 0), true);
			else if (cFrom.x - cTo.x == 2)
				move(pieceAt(cTo.add(-2, 0)), cFrom.add(-1, 0), true);
			((King) piece).moved = true;
		} else if (piece instanceof Rook) ((Rook) piece).moved = true;
		else if (piece instanceof Pawn) {
			((Pawn) piece).moved = true;
			if ((white && cTo.y - cFrom.y == 2)
					|| (!white && cTo.y - cFrom.y == -2)) ((Pawn) piece).lastEnPassant = true;
			if (cTo.x != cFrom.x) removePiece(new Coordinate(cTo.x, cFrom.y));
			if (cTo.y == 7 || cTo.y == 0) {
				if (queen || inAr.length == 2) {
					Queen q = new Queen(this, cTo, piece.isWhite());
					addPiece(q, q.coordinate);
				} else if (bishop) {
					Bishop b = new Bishop(this, cTo, piece.isWhite());
					addPiece(b, b.coordinate);
				} else if (knight) {
					Knight k = new Knight(this, cTo, piece.isWhite());
					addPiece(k, k.coordinate);
				} else if (rook) {
					Rook r = new Rook(this, cTo, piece.isWhite());
					addPiece(r, r.coordinate);
				}
			}
		}
		if (inCheckmate(!white)) setWin(white);
		else if (!canMove(!white)) {
			setWin(white);
			setWin(!white);
		}
		return true;
	}

	/**
	 * Marks a player as a winner
	 *
	 * @param white True if you want white to win, false for black
	 */
	private void setWin(boolean white) {
		if (white) wWin = true;
		else bWin = true;
	}

	/**
	 * Tries to move a piece to a coordinate, following all rules of chess
	 *
	 * @param piece      The piece to move
	 * @param coordinate The location to move the piece to
	 * @return Whether the move was successful
	 */
	public boolean move(Piece piece, Coordinate coordinate) {
		return move(piece, coordinate, false);
	}

	/**
	 * Tries to move a piece to a coordinate, with the option to ignore some rules
	 *
	 * @param piece            The piece to move
	 * @param coordinate       The location to move the piece to
	 * @param ignoreLegalCheck If true, allows you to move a piece illegally
	 * @return Whether the move was successful
	 */
	public boolean move(Piece piece, Coordinate coordinate, boolean ignoreLegalCheck) {
		if (!coordinate.isValid()) return false;
		if (!ignoreLegalCheck && !piece.legalMoves().contains(coordinate)) return false;
		undoCoord = piece.coordinate;
		undoPiece = piece;
		undoKilled = pieceAt(coordinate);
		addPiece(piece, coordinate);
		removePiece(undoCoord);
		return true;
	}

	/**
	 * Marks that no pawns of a color can be taken by en passant
	 *
	 * @param white True if the color is white, false if black
	 */
	public void clearEnPassant(boolean white) {
		for (Piece piece : getPieces(white)) if (piece instanceof Pawn) ((Pawn) piece).lastEnPassant = false;
	}

	/**
	 * Undoes the last move, used mostly to check if a move would put a king in check
	 */
	public void undo() {
		if (undoKilled == null) removePiece(undoPiece.coordinate);
		else addPiece(undoKilled);
		addPiece(undoPiece, undoCoord);
		undoCoord = null;
		undoKilled = null;
		undoPiece = null;
	}

	/**
	 * Gets the piece at a coordinate
	 *
	 * @param coordinate The location of the piece to get on the board
	 * @return The piece at coordinate, null if the space is empty
	 */
	public Piece pieceAt(Coordinate coordinate) {
		if (!coordinate.isValid()) return null;
		return pieces[coordinate.x][coordinate.y];
	}

	/**
	 * Gets all pieces on the board of a certain color
	 *
	 * @param white True to get white pieces, false to get black pieces
	 * @return A list of all pieces of the specified color
	 */
	public ArrayList<Piece> getPieces(boolean white) {
		ArrayList<Piece> pList = new ArrayList<>();
		for (Piece[] pieces : this.pieces)
			for (Piece p : pieces)
				if (p != null && p.isWhite() == white) pList.add(p);
		return pList;
	}

	/**
	 * Returns all of the pieces on the board
	 *
	 * @return A list of all pieces
	 */
	public ArrayList<Piece> getPieces() {
		ArrayList<Piece> pList = new ArrayList<>();
		for (Piece[] pieces : this.pieces)
			for (Piece p : pieces)
				if (p != null) pList.add(p);
		return pList;
	}

	/**
	 * Gets the king of a specific color
	 *
	 * @param white True to get white king, false to get black king
	 * @return The king of the specified color
	 */
	public King getKing(boolean white) {
		if (white) return wKing;
		else return bKing;
	}

	/**
	 * Checks if a player can move
	 *
	 * @param white True to check the white player's pieces, false to check the black player's pieces
	 * @return Whether the specified player can move
	 */
	public boolean canMove(boolean white) {
		for (Piece p : getPieces(white)) if (!p.legalMoves().isEmpty()) return true;
		return false;
	}

	/**
	 * Checks if a player is in check
	 *
	 * @param white True to check the white player, false to check the black player
	 * @return Whether the specified player is in check
	 */
	public boolean inCheck(boolean white) {
		return getKing(white).inDanger();
	}

	/**
	 * Checks if a player is in checkmate
	 *
	 * @param white True to check the white player, false to check the black player
	 * @return Whether the specified player is in checkmate
	 */
	public boolean inCheckmate(boolean white) {
		return inCheck(white) && !canMove(white);
	}

	/**
	 * Adds a piece to the board at the specified coordinate
	 *
	 * @param piece      The piece to add
	 * @param coordinate The coordinate to add the piece at
	 * @return Whether or not the coordinate was valid
	 */
	public boolean addPiece(Piece piece, Coordinate coordinate) {
		if (!coordinate.isValid()) return false;
		pieces[coordinate.x][coordinate.y] = piece;
		piece.coordinate = coordinate;
		return true;
	}

	/**
	 * Adds a piece to the board at the coordinate in the piece variable
	 *
	 * @param piece      The piece to add
	 * @return Whether or not the piece's coordinate was valid
	 */
	public boolean addPiece(Piece piece) {
		return addPiece(piece, piece.coordinate);
	}

	/**
	 * Removes a piece from the board at the specified coordinate
	 *
	 * @param coordinate The coordinate to remove the piece from
	 * @return Whether or not the coordinate was valid
	 */
	public boolean removePiece(Coordinate coordinate) {
		if (!coordinate.isValid()) return false;
		pieces[coordinate.x][coordinate.y] = null;
		return true;
	}

	/**
	 * Gets the ASCII art depiction of the board
	 *
	 * @return The board in a format that can be printed
	 */
	public String toString() {
		String s = "";
		for (int i = 8; i > 0; i--) {
			for (char c = 'a'; c <= 'h'; c++) {
				Piece piece = pieceAt(new Coordinate(c, i));
				if (piece == null) {
					if (i % 2 == 0)
						if (c % 2 == 0) s += "##";
						else s += "  ";
					else if (c % 2 == 0) s += "  ";
					else s += "##";
				} else {
					if (piece.isWhite()) s += "w";
					else s += "b";
					if (piece instanceof Pawn) s += "p";
					else if (piece instanceof Bishop) s += "B";
					else if (piece instanceof King) s += "K";
					else if (piece instanceof Knight) s += "N";
					else if (piece instanceof Queen) s += "Q";
					else if (piece instanceof Rook) s += "R";
					else s += "?";
				}
				s += " ";
			}
			s += i + "\n";
		}
		for (char c = 'a'; c <= 'h'; c++) s += " " + c + " ";
		return s.substring(0, s.length() - 1);
	}
}
