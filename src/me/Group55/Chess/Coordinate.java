package me.Group55.Chess;

/**
 * The locations on the board
 * Used to help hide the differences between the x,y position in a matrix and column, row notation
 *
 * @author Mason Grosset
 * @author Jared Neal
 */
public class Coordinate {
	/**
	 * The row of the coordinate, mainly used for printing debug messages
	 */
	public final int row;

	/**
	 * The column of the coordinate, mainly used for printing debug messages
	 */
	public final char col;

	/**
	 * The x location on the board in the 8*8 matrix of pieces
	 */
	public final int x;

	/**
	 * The y location on the board in the 8*8 matrix of pieces
	 */
	public final int y;

	/**
	 * Creates a coordinate from column, row notation
	 * Calculates x and y
	 *
	 * @param col The column on the board (a-h)
	 * @param row The row on the board (1-8)
	 */
	public Coordinate(char col, int row) {
		this.row = row;
		this.col = col;
		this.x = col - 'a';
		this.y = row - 1;
	}

	/**
	 * Creates a coordinate from the location on the 8*8 piece matrix
	 * Calculates row and col
	 *
	 * @param x The x coordinate on the board (0-7)
	 * @param y The y coordinate on the board (0-7)
	 */
	public Coordinate(int x, int y) {
		this.row = y + 1;
		this.col = (char) (x + 'a');
		this.x = x;
		this.y = y;
	}

	/**
	 * Returns a coordinate plus x, y
	 *
	 * @param x The x value to add to this coordinate
	 * @param y The y value to add to this coordinate
	 * @return A copy of this coordinate + (x,y)
	 */
	public Coordinate add(int x, int y) {
		return new Coordinate(this.x + x, this.y + y);
	}

	/**
	 * Checks whether the coordinate is a valid coordinate on the board
	 *
	 * @return 0 less than or equal to x less than or equal to 7 and 0 less than or equal to y less than or equal to 7
	 */
	public boolean isValid() {
		return x >= 0 && x < 8 && y >= 0 && y < 8;
	}

	/**
	 * Checks whether two Coordinates have the same values
	 *
	 * @param o The object to compare to
	 * @return Both coordinates have the same x and y
	 */
	public boolean equals(Object o) {
		return o instanceof Coordinate && ((Coordinate) o).x == x && ((Coordinate) o).y == y;
	}

	/**
	 * Returns a readable format of the coordinate
	 * @return The coordinate in the format: a3: (0,2)
	 */
	@Override
	public String toString() {
		return col + "" + row + ": " + "(" + x + ", " + y + ")";
	}
}
