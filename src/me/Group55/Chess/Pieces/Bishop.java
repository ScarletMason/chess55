package me.Group55.Chess.Pieces;

import me.Group55.Chess.Board;
import me.Group55.Chess.Coordinate;

import java.util.List;

/**
 * Bishop piece.
 *
 * @author Jared Neal
 * @author Mason Grosset
 *
 */

public class Bishop extends Piece {
	/**
	 * Calls piece constructor.
	 * @param board 		board the piece is on
	 * @param coordinate	coordinate to put the piece on
	 * @param white			color of the piece
	 */
	public Bishop(Board board, Coordinate coordinate, boolean white) {
		super(board, coordinate, white);
	}

	/**
	 * Calls moveAlongCross() since that's the pattern the bishop can move in
	 * @return A list of coordinates that the piece can move to
	 */
	@Override
	List<Coordinate> possibleMoves() {
		//TODO
		return movesAlongCross(this);
	}
}
