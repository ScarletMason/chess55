package me.Group55.Chess.Pieces;

import me.Group55.Chess.Board;
import me.Group55.Chess.Coordinate;

import java.util.ArrayList;
import java.util.List;

/**
 *  This class is designed to be a parent to all the various pieces in chess. Each piece shares several traits despite implementing
 *  different methods of movement. It implements methods that check for possible moves as well as the validity of moves.
 *
 * @author Mason Grosset
 * @author  Jared Neal
 */

public abstract class Piece {
	/**
	 * The board that the piece is apart of
	 */
	public final Board board;
	/**
	 * The coordinate of the piece on the board
	 */
	public Coordinate coordinate;
	/**
	 * The color of the piece
	 */
	private final boolean white;

	/**
	 * Constructor that sets all parameters to the piece's components
	 * @param board 		The board that the piece will be on
	 * @param coordinate	The coordinate to set the piece to
	 * @param white			Sets the piece to either white or black
	 */
	public Piece(Board board, Coordinate coordinate, boolean white) {
		this.board = board;
		this.coordinate = coordinate;
		this.white = white;
	}

	/**
	 * Checks to see if the piece is white or black
	 * @return returns the white property of the piece
	 */
	public boolean isWhite() {
		return white;
	}

	/**
	 * Abstract function that allows for different implementations of moves.
	 *
	 * <p> Depending on which piece is trying to be moved, they have different movement options from one another. Rather than handling every piece
	 * in one big method, each Piece subclass implements their own possibleMoves() method</p>
	 * @return No matter what piece is implementing this method, the return value will always be a list of coordinates
	 * that are potential, but not legal, moves
	 */
	abstract List<Coordinate> possibleMoves();

	/**
	 * Uses possibleMoves() to generate a list to iterate through and find valid moves
	 *
	 * <p>On top of just checking normal move validity, this method handles checking to make sure that no matter what piece is moving,
	 * king is not put in danger as a result of said movement. This is seen inside the loop when it checks to make sure that
	 * after moving the king piece remains safe.</p>
	 * @return After going through all possible moves, a list of actual possible moves are returned.
	 */
	public List<Coordinate> legalMoves() {
		//TODO Filter out moves that put the king in check
		//TODO Try to move, check if your king is in danger, undo move
		//TODO Check that x and y coordinates are valid
		List<Coordinate> moves = possibleMoves();
		List<Coordinate> legal = new ArrayList<>();
		King king = board.getKing(white);
		for(Coordinate c: moves){
			if(c.isValid()){
				board.move(this, c, true);
				if(!king.inDanger()) legal.add(c);
				board.undo();
			}
		}
		return legal;
	}

	/**
	 * This is a helper method to see how far a piece can go in a certain direction.
	 *
	 * <p>In each cycle of the loop, the spot on the board is checked, and if it's empty, the coordinate is updated
	 * and the loop continues until it either hits another piece or the end of the board. In that case, it checks
	 * if the piece at the coordinate is an enemy to see if the piece can be taken. Otherwise, the loop is broken and
	 * the coordinates returned.</p>
	 *
	 * @param piece		The piece being moved
	 * @param x			Amount to move in the x direction
	 * @param y			Amount to move in the y direction
	 * @return			List of coordinates that can be moved to
	 */
	private static List<Coordinate> movesAlongOffset(Piece piece, int x, int y) {
		//TODO Checks if piece -> (piece.x + x, piece.y + y) is valid
		//TODO   Then if piece -> (piece.x + x + x, piece.y + y + y) is valid
		//TODO   Then ... until it reaches the end of the board
		Coordinate temp = piece.coordinate.add(x,y);
		List<Coordinate>  coords = new ArrayList<>();
		while(temp.isValid()){
			if(piece.board.pieceAt(temp) == null){
				coords.add(temp);
				temp = temp.add(x,y);
			}else{
				Piece ptemp = piece.board.pieceAt(temp);
				if(ptemp.isWhite() != piece.isWhite()) coords.add(temp);
				break;
			}
		}
		return coords;
	}

	/**
	 * Checks north, south, east, and west directions of a piece to see how far they can move in all directions.
	 *
	 * <p>The four calls to moveAlongOffset are the four different directions being checked. As explained above,
	 * the highest possible coordinate in each direction will be logged in the list from the moveAlongOffset function,
	 * and all four of those lists are compiled in the list in this method.</p>
	 * @param piece		Piece that is trying to be moved
	 * @return			List of all the spots that piece could go in a plus shape
	 */
	static List<Coordinate> movesAlongPlus(Piece piece) {
		ArrayList<Coordinate> list = new ArrayList<>();
		list.addAll(movesAlongOffset(piece, 0, 1));
		list.addAll(movesAlongOffset(piece, 1, 0));
		list.addAll(movesAlongOffset(piece, 0, -1));
		list.addAll(movesAlongOffset(piece, -1, 0));
		return list;
	}

	/**
	 * Checks northeast, northwest, southeast, and southwest directions of a piece to see how far they can move in all directions.
	 * <p>The four calls to moveAlongOffset are the four different directions being checked. Exactly like movesAlongPlus, the highest values
	 * from the four calls to moveAlongOffset are compiled into one list</p>
	 * @param piece		Piece that is trying to be moved
	 * @return			List of all the spots that piece could go in an X shape
	 */
	static List<Coordinate> movesAlongCross(Piece piece) {
		ArrayList<Coordinate> list = new ArrayList<>();
		list.addAll(movesAlongOffset(piece, 1, 1));
		list.addAll(movesAlongOffset(piece, -1, -1));
		list.addAll(movesAlongOffset(piece, 1, -1));
		list.addAll(movesAlongOffset(piece, -1, 1));
		return list;
	}
}
