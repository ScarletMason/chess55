package me.Group55.Chess.Pieces;

import me.Group55.Chess.Board;
import me.Group55.Chess.Coordinate;

import java.util.ArrayList;
import java.util.List;

/**
 * King piece.
 *
 * @author Jared Neal
 * @author Mason Grosset
 */

public class King extends Piece {
	/**
	 * boolean to check if the King has moved at all
	 */
	public boolean moved = false;
	/**
	 * Calls piece constructor.
	 * @param board 		board the piece is on
	 * @param coordinate	coordinate to put the piece on
	 * @param white			color of the piece
	 */
	public King(Board board, Coordinate coordinate, boolean white) {
		super(board, coordinate, white);
	}

	/**
	 * Implementation of possibleMoves() that allows the king to not just move, but to also castle.
	 * <p>In order to check for a castle, the 3 spaces to the left and 2 to the right are checked to make sure
	 * they are empty and neither move would put the king in danger. If it doesn't, it's added to the list
	 * of possible moves. Otherwise movement in all 8 directions is checked.</p>
	 * @return List of coordinates for the king to potentially move to.
	 */
	@Override
	List<Coordinate> possibleMoves() {
		//TODO
		//TODO don't forget castling
		//TODO don't forget, can't castle through check
		//make new coordinate consisting of current coordinate -4 on x, 0 y and one for + 3 x
		List<Coordinate> moves = new ArrayList<>();
		Coordinate qcoord = this.coordinate.add(-4, 0);
		Coordinate kcoord = this.coordinate.add(3, 0);
		boolean qDanger = false;
		boolean kDanger = false;
		Piece qpot = board.pieceAt(qcoord);
		Piece kpot = board.pieceAt(kcoord);
		if (inDanger()) {
			kDanger = true;
			qDanger = true;
		}
		board.move(this, coordinate.add(1, 0), true);
		if (inDanger()) kDanger = true;
		board.undo();
		board.move(this, coordinate.add(-1, 0), true);
		board.undo();
		if (inDanger()) qDanger = true;
		for (int i = -3; i < 3; i++) {
			if (i == 0) continue;
			Piece temp = board.pieceAt(coordinate.add(i, 0));
			if (temp != null) {
				if (i < 0) qDanger = true;
				else kDanger = true;
			}
		}
		if (qpot instanceof Rook && !((Rook) qpot).moved && !moved && !qDanger) {
			Coordinate temp = coordinate.add(-2, 0);
			moves.add(temp);
		}
		if (kpot instanceof Rook && !((Rook) kpot).moved && !moved && !kDanger) {
			Coordinate temp = coordinate.add(2, 0);
			moves.add(temp);
		}
		for (int i = -1; i < 2; i++) {
			for (int j = -1; j < 2; j++) {
				if (i == 0 && j == 0) continue;
				Piece temp = board.pieceAt(coordinate.add(i, j));
				if ((temp == null || temp.isWhite() != isWhite())) moves.add(coordinate.add(i, j));
			}
		}
		return moves;
	}

	/**
	 * Checks all the different conditions in which the King piece can be in danger.
	 * @return	True if any condition is met, false if the move would not put the king in danger.
	 */
	public boolean inDanger() {
		for (Coordinate c : Piece.movesAlongCross(this)) {
			Piece p = board.pieceAt(c);
			if (p != null && p.isWhite() != isWhite() && (p instanceof Bishop || p instanceof Queen ||
					(p instanceof Pawn && p.possibleMoves().contains(coordinate)))) return true;
		}
		for (Coordinate c : Piece.movesAlongPlus(this)) {
			Piece p = board.pieceAt(c);
			if (p != null && p.isWhite() != isWhite() && (p instanceof Rook || p instanceof Queen)) return true;
		}
		for (Coordinate c : Knight.knightHit(this)) {
			Piece p = board.pieceAt(c);
			if (p != null && p.isWhite() != isWhite() && p instanceof Knight) return true;
		}
		for (int i = -3; i < 3; i++) {
			if (i == 0) continue;
			Piece temp = board.pieceAt(coordinate.add(i, 0));
			if (temp instanceof King) return true;
		}
		return false;
	}
}
