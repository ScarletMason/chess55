package me.Group55.Chess.Pieces;

import me.Group55.Chess.Board;
import me.Group55.Chess.Coordinate;

import java.util.List;

/**
 * Queen Piece.
 *
 * @author Mason Grosset
 * @author Jared Neal
 */

public class Queen extends Piece {
	/**
	 * Calls piece constructor.
	 * @param board 		board the piece is on
	 * @param coordinate	coordinate to put the piece on
	 * @param white			color of the piece
	 */
	public Queen(Board board, Coordinate coordinate, boolean white) {
		super(board, coordinate, white);
	}

	/**
	 * Queen moves the same way as the movesAlongPlus and movesAlongCross functions are designed,
	 * so a list of those two methods lists is all that is needed.
	 * @return	List of all coordinates from the two aforementioned move functions
	 */
	@Override
	List<Coordinate> possibleMoves() {
		//TODO
		List<Coordinate> moving = movesAlongPlus(this);
		moving.addAll(movesAlongCross(this));
		return moving;
	}
}
