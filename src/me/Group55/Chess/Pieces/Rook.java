package me.Group55.Chess.Pieces;

import me.Group55.Chess.Board;
import me.Group55.Chess.Coordinate;

import java.util.List;

/**
 * Rook piece.
 * @author Mason Grosset
 * @author Jared Neal
 */

public class Rook extends Piece {
	public boolean moved = false;
	/**
	 * Calls piece constructor.
	 * @param board 		board the piece is on
	 * @param coordinate	coordinate to put the piece on
	 * @param white			color of the piece
	 */
	public Rook(Board board, Coordinate coordinate, boolean white) {
		super(board, coordinate, white);
	}

	/**
	 * The rook moves in the same way that the movesAlongPLus() method is written, so it just makes a list out of
	 * the values returned from it.
	 * @return	List of values from the movesAlongPlus() method
	 */
	@Override
	List<Coordinate> possibleMoves() {
		//TODO
		return movesAlongPlus(this);
	}
}
