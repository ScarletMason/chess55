package me.Group55.Chess.Pieces;

import me.Group55.Chess.Board;
import me.Group55.Chess.Coordinate;

import java.util.ArrayList;
import java.util.List;

/**
 * Pawn piece.
 *
 * @author Mason Grosset
 * @author Jared Neal
 */

public class Pawn extends Piece {
	/**
	 * boolean that is true if the most recent move a pawn made was its special case where it moves forward 2
	 */
	public boolean lastEnPassant = false;
	/**
	 * boolean that is false until the pawn moves
	 */
	public boolean moved = false;

	/**
	 * Calls piece constructor.
	 * @param board 		board the piece is on
	 * @param coordinate	coordinate to put the piece on
	 * @param white			color of the piece
	 */
	public Pawn(Board board, Coordinate coordinate, boolean white) {
		super(board, coordinate, white);
	}

	/**
	 * Movement for pawns as well as EnPassant.
	 * <p>Handles movement vertically as well as attacking diagonally and moving to that space. By checking if there is a pawn next
	 * to the piece and if that adjacent pawn's lastEnPassant value, the validity of an EnPassant move is checked and either added
	 * to the list of coordinates or is not.</p>
	 * @return	List of coordinates that the pawn can move to
	 */
	@Override
	List<Coordinate> possibleMoves() {
		//TODO Use coordinates instead of pieces for temps
		List<Coordinate> moves = new ArrayList<>();
		Coordinate temp, temp2;
		if (isWhite()) {
			temp = coordinate.add(0, 2);
			temp2 = coordinate.add(0, 1);
		} else {
			temp = coordinate.add(0, -2);
			temp2 = coordinate.add(0, -1);
		}

		if (board.pieceAt(temp2) == null) {
			moves.add(temp2);
			if (!moved && board.pieceAt(temp) == null) moves.add(temp);
		}

		if (isWhite()) {
			temp = coordinate.add(-1, 1);
			if (board.pieceAt(temp) != null && board.pieceAt(temp).isWhite() != isWhite()) moves.add(temp);
			temp = coordinate.add(1, 1);
			if (board.pieceAt(temp) != null && board.pieceAt(temp).isWhite() != isWhite()) moves.add(temp);
			temp = coordinate.add(-1, 0);
			if (board.pieceAt(temp) != null && board.pieceAt(temp).isWhite() != isWhite() && board.pieceAt(temp) instanceof Pawn && ((Pawn) board.pieceAt(temp)).lastEnPassant)
				moves.add(temp.add(0, 1));
			temp = coordinate.add(1, 0);
			if (board.pieceAt(temp) != null && board.pieceAt(temp).isWhite() != isWhite() && board.pieceAt(temp) instanceof Pawn && ((Pawn) board.pieceAt(temp)).lastEnPassant)
				moves.add(temp.add(0, 1));
		} else {
			temp = coordinate.add(-1, -1);
			if (board.pieceAt(temp) != null && board.pieceAt(temp).isWhite() != isWhite()) moves.add(temp);
			temp = coordinate.add(1, -1);
			if (board.pieceAt(temp) != null && board.pieceAt(temp).isWhite() != isWhite()) moves.add(temp);
			temp = coordinate.add(-1, 0);
			if (board.pieceAt(temp) != null && board.pieceAt(temp).isWhite() != isWhite() && board.pieceAt(temp) instanceof Pawn && ((Pawn) board.pieceAt(temp)).lastEnPassant)
				moves.add(temp.add(0, -1));
			temp = coordinate.add(1, 0);
			if (board.pieceAt(temp) != null && board.pieceAt(temp).isWhite() != isWhite() && board.pieceAt(temp) instanceof Pawn && ((Pawn) board.pieceAt(temp)).lastEnPassant)
				moves.add(temp.add(0, -1));
		}
		return moves;
	}
}
