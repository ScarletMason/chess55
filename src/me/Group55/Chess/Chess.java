package me.Group55.Chess;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;

/**
 * Main class
 * Creates a board, and sends the board read inputs
 * Sends output to terminal
 *
 * @author Mason Grosset
 * @author Jared Neal
 */
public class Chess {

	/**
	 * Creates a board
	 * If debug is on, calls test with a list of strings that simulates playing
	 * calls input which handles standard input and output operations
	 *
	 * @param args is ignored
	 */
	public static void main(String[] args) {
		Board b = new Board();
		boolean debug = false;
		boolean white = true;
		if (debug) {
			//K Castle: white = test(b, Arrays.asList("a2 a3", "g7 g6", "g2 g3", "f8 g7", "g3 g4", "g8 f6", "f2 f3", "e8 g8"));
			//Q Castle: white = test(b, Arrays.asList("e2 e3", "a7 a6", "d1 e2", "a6 a5", "b2 b3", "a5 a4", "c1 c2", "a4 a3", "c1 b2", "a4 b3", "c1 b2", "b3 a2", "b1 c3", "b3 a2", "e1 c1"));
			//Fool's Mate: white = test(b, Arrays.asList("f2 f3", "e7 e5", "g2 g4"));
			//Check: white = test(b, Arrays.asList("f2 f3", "e7 e5", "g2 g4", "a7 a6", "f1 g2"));
		} else System.out.println(b+"\n");
		input(b, white);
	}

	/**
	 * Simulates a list of input commands, has limited output capability
	 *
	 * @param b          The board that the inputs are being sent to
	 * @param stringList The list of input strings to run on the board
	 * @return Who's next to move
	 */
	public static boolean test(Board b, List<String> stringList) {
		boolean white = true;
		for (String string : stringList) {

			if (b.tryMove(string, white)) {

				white = !white;
				System.out.println(b);
			} else System.out.println("Illegal move, try again");
		}
		System.out.println(b + "\n");
		return white;
	}

	/**
	 * Handles all standard inputs for a board
	 * Sends appropriate outputs
	 *
	 * @param b     The board the game is being played on
	 * @param white Is if the first player should start on white, used for testing
	 */
	public static void input(Board b, boolean white) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		// Reading data using readLine
		while (!b.bWin && !b.wWin) {
			String input = "";
			try {
				if (white) System.out.print("White");
				else System.out.print("Black");
				System.out.print("'s move: ");
				input = reader.readLine();
				System.out.println();
			} catch (IOException e) {
				e.printStackTrace();
			}
			if (b.tryMove(input, white)) {
				boolean bMate = b.inCheckmate(false);
				boolean wMate = b.inCheckmate(true);
				if (wMate || bMate) {
					System.out.println(b + "\n");
					System.out.println("Checkmate\n");
				} else if ((!b.bWin && !b.wWin) || (b.bWin && b.wWin && !b.draw)) System.out.println(b + "\n");
				if (b.wWin) {
					if (!b.bWin) {
						System.out.print("White wins");
					} else if (b.draw) System.out.print("draw");
					else System.out.println("Stalemate\n\ndraw");
				} else if (b.bWin) {
					System.out.print("Black wins");
				} else if (b.inCheck(!white)) System.out.println("Check\n");
				white = !white;
			} else System.out.println("Illegal move, try again" + "\n");
		}
	}
}
